package Practics_Module_2.homework;

import Practics_Module_2.homework.controller.Command;
import Practics_Module_2.homework.view.ConsoleHelper;
import java.io.IOException;
import java.sql.SQLException;

public class ProgramRunner {

    public static void main(String[] args) throws SQLException, IOException {

        Integer choice = -1;
        Command command = new Command();
        ConsoleHelper.writeMessage("Вас приветствует система CRUD\n");
        while (choice != 0) {
            ConsoleHelper.writeMessage("Выберите действие, которое хотите осуществить: (для выхода введите '0')\n" +
            "1 - Показать записи по условию." + '\t' + "2 - Обновить данные." + '\t' + "3 - Вставить данные." + '\t' + "4 - Удалить данные.\n");
            ConsoleHelper.writeMessage("Ввод: ");
            choice = ConsoleHelper.readInt();
            if (choice != null) {
                ConsoleHelper.writeMessage("Вы выбрали пункт " + choice + '\n');
                if (choice == 0)
                    return;
                command.executeByNumber(choice);
            }else
                break;
        }
    }
}
