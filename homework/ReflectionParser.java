package Practics_Module_2.homework;

import Practics_Module_2.homework.model.Model;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by Иван on 04.12.2016.
 */
public class ReflectionParser {


    public static Map<String,Object> parse(Model model) {
        Set<String> fields = parseFieldsName(model);
        Method[] methods = model.getClass().getMethods();
        Object value = null;
        Map<String,Object> result = new TreeMap<>();

        try {
            for (String fieldName : fields) {
                    for (Method m : methods) {
                        if (m.getName().toLowerCase().contains(fieldName) && !m.getName().contains("set")) {
                            m.setAccessible(true);
                            value = m.invoke(model,new Object[0]);
                            break;
                        }
                    }
                    result.put(fieldName,value);
            }
            return result;
        }catch (IllegalAccessException | InvocationTargetException e){
            e.printStackTrace();
            return null;
        }
    }


    public static Set<String> parseFieldsName(Model model) {
        return parseFieldsName(model.getClass());
    }

    public static Set<String> parseFieldsName(Class<? extends Model> modelClass){
        Field[]fields = modelClass.getDeclaredFields();
        Set<String> result = new HashSet<>();

        for (Field f : fields)
            if (f.isAnnotationPresent(Practics_Module_2.homework.model.Field.class))
                result.add(f.getName());

        return result;
    }
}
