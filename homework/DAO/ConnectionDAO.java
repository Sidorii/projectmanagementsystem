package Practics_Module_2.homework.DAO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.*;

public class ConnectionDAO {

    public static final String DATA_BASE_URL = "jdbc:mysql://localhost/homeworkgoit?useSSL=false";
    public static final String DRIVER_NAME =  "com.mysql.jdbc.Driver";

    private static String DB_URL;
    private static String DB_DRIVER;
    private static final Logger logger = LoggerFactory.getLogger(ConnectionDAO.class);;


    ConnectionDAO(String url, String driverName){
        DB_URL = url;
        DB_DRIVER = driverName;
        try{
            Class.forName(DB_DRIVER);
        }catch (ClassNotFoundException e){
            logger.error("Not found class driver: " + DB_DRIVER + " for data base" + e);
        }
    }

    Connection getConnection() throws SQLException{
        return DriverManager.getConnection(DB_URL,"root","root");
    }









}
