package Practics_Module_2.homework.DAO;

import Practics_Module_2.homework.ReflectionParser;
import Practics_Module_2.homework.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.security.InvalidParameterException;
import java.sql.*;
import java.util.*;


public class DAO {


    private ConnectionDAO connectionDAO;
    private Logger logger = LoggerFactory.getLogger(DAO.class);

    public DAO(String url, String driver) {
        connectionDAO = new ConnectionDAO(url,driver);
    }

    //CRUD interface

    public void insert(Model model){

        String parameters = generateParametersOfTable(model);

        String sql = "INSERT INTO " + model.getTable().name() + " VALUES(" + parameters + ")";

        try(Connection connection = connectionDAO.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql))
        {
            model.initializeStatementParameters(preparedStatement);
            preparedStatement.execute();
            logger.info("Company " + model + " successfully inserted into DB");
        }catch (SQLException e){
            logger.error(model + " was NOT inserted into DB. Exception is: " + e);
        }
    }


    public Collection<Model> read(Model m){

        List<Model> result = new ArrayList<>();
        Map<String,Object> fields = ReflectionParser.parse(m);
        String sql = "SELECT * FROM " + m.getTable().name() +  generateWhere(fields);

        try(Connection connection = connectionDAO.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql)) {

            Model model;

            while (resultSet.next()) {
                model = m.createInstance(resultSet);
                result.add(model);
            }
            logger.info("Data selected successfully");
            return result;
        }catch (SQLException e){
            logger.error("Data was NOT selected from DB. Exception is: " + e);
            return result;
        }
    }

    private String generateParametersOfTable(Model model){
       int count = model.getTable().getNumberOfFields();
        String result = "?";
        for (int i = 0; i < count-1; i++)
            result += ",?";
        return result;
    }


    public void update(Model condition, Model newEntry){

        if (condition.getClass() != newEntry.getClass())
            return;

        Map<String,Object> entryField = ReflectionParser.parse(newEntry);

        Map<String,Object> conditionFields = ReflectionParser.parse(condition);

        String querySET;
        String queryWHERE;


        querySET = generateSetForUpdate(entryField);



        queryWHERE = generateWhere(conditionFields);


        String sql  = "UPDATE " + newEntry.getTable().name() +  querySET  + queryWHERE;

        try(Connection connection = connectionDAO.getConnection();
            Statement statement = connection.createStatement())
        {
            int upd = statement.executeUpdate(sql);
            logger.info("Table updated successfully. " + upd + " rows was changed.");
        }catch (SQLException e){
            logger.error("Table was NOT updated. Exception is: " + e);
        }
    }

    public void delete(Model condition) {

        Map<String,Object> fields = ReflectionParser.parse(condition);

        String where = generateWhere(fields);
        String sql = "DELETE FROM " + condition.getTable().name() + where;

        try(Connection connection = connectionDAO.getConnection();
            Statement statement = connection.createStatement())
        {
            int dlt = statement.executeUpdate(sql);
            logger.info(dlt + " entries successfully deleted from DB");
        }catch (SQLException e){
            logger.error("Entries was NOT deleted. Exception is: " + e);
        }
    }

    private String generateWhere(Map<String,Object> conditionFields){

        String queryWHERE = " WHERE ";

        for (String entry:conditionFields.keySet()){
            if(conditionFields.get(entry) != null)
                queryWHERE += entry + " = '" + conditionFields.get(entry) + "' AND ";
        }
        if (queryWHERE.length() <= 11)
            return "";

        if (queryWHERE.length() > 6)
            queryWHERE = queryWHERE.substring(0,queryWHERE.length()-4);

        return queryWHERE;
    }

    private String generateSetForUpdate(Map<String,Object> entryField){
        String querySET = " SET ";

        for (String entry:entryField.keySet()){
            if(entryField.get(entry) != null)
                querySET += entry + " = '" + entryField.get(entry) + "', ";
        }

        if (querySET.length() <= 7)
            throw new InvalidParameterException("Parameter that represent newEntry is wrong");

        querySET = querySET.substring(0,querySET.length()-2);
        return querySET;
    }
}
