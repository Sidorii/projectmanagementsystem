package Practics_Module_2.homework.controller;

import Practics_Module_2.homework.DAO.ConnectionDAO;
import Practics_Module_2.homework.DAO.DAO;
import Practics_Module_2.homework.ReflectionParser;
import Practics_Module_2.homework.model.*;
import Practics_Module_2.homework.model.Field;
import Practics_Module_2.homework.view.ConsoleHelper;

import java.io.IOException;
import java.lang.reflect.*;
import java.sql.SQLException;
import java.util.*;
import static Practics_Module_2.homework.view.ConsoleHelper.writeMessage;
import static Practics_Module_2.homework.view.ConsoleHelper.readInt;

/**
 * Created by Иван on 04.12.2016.
 */
public class Command {

    private DAO dao = new DAO(ConnectionDAO.DATA_BASE_URL,ConnectionDAO.DRIVER_NAME);

    public void executeByNumber(int number) throws SQLException, IOException {
        writeMessage("Пожалуйста, выберите сущность с которой хотите работать:\n " +
                "1 - Компании" + '\t' + "2 - Инвесторы " + '\t' + "3 - Pазработчики" + '\t' +
                "4 - Проекты " + '\t' + "5 - Навыки разработчкиов" + "\t0 - Вернуться обратно");
        writeMessage("\nВвод: ");
        Integer choice = readInt();
        if (choice != null){
            writeMessage("Вы выбрали пункт " + choice + '\n');
            if(choice == 0)
                return;
            switch (number){
                case 1:
                    executeSelect(choice);
                    break;
                case 2:
                    executeUpdate(choice);
                    break;
                case 3:
                    executeInsert(choice);
                    break;
                case 4:
                    executeDelete(choice);
                    break;
                default:
                    writeMessage("Введены некоректные данные.");
                    break;
            }
        }

    }

    public void executeSelect(int number) throws SQLException, IOException {
        Model model = getInstanceByID(number);
        Collection<Model> result = dao.read(model);
        result.forEach((m)->System.out.println(m));
    }

    public void executeUpdate(int number) throws IOException, SQLException {
        writeMessage("Задайте условие для обновляемых полей\n");
        Model condition = getInstanceByID(number);
        writeMessage("Введите новые данные\n");
        Model entry = getInstanceByID(number);
        dao.update(condition,entry);
    }

    public void executeInsert(int number) throws IOException, SQLException {
        Model model = getInstanceByID(number);
        dao.insert(model);
    }

    public void executeDelete(int number) throws SQLException, IOException {
        Model model = getInstanceByID(number);
        dao.delete(model);
    }

    private Model getInstanceByID(int id){
        writeMessage("Введите данные в нужные вам поля. (Поля, которые не учавствуют в запросе, можно пропустить нажав `Enter`)");
        try {
            switch (id) {
                case 1:
                    return setFieldsForInstance(Company.class);
                case 2:
                    return setFieldsForInstance(Customer.class);
                case 3:
                    return setFieldsForInstance(Developer.class);
                case 4:
                    return setFieldsForInstance(Project.class);
                case 5:
                    return setFieldsForInstance(Skill.class);
                default:
                    return null;
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    private Model setFieldsForInstance(Class<? extends Model> _class) throws IllegalAccessException, InstantiationException, InvocationTargetException {

        java.lang.reflect.Field[] fields = _class.getDeclaredFields();

        Model model = _class.newInstance();

        Method methods[] = ConsoleHelper.class.getDeclaredMethods();

        ConsoleHelper ch = new ConsoleHelper();
        for (java.lang.reflect.Field field:fields){
            for(Method method:methods){
                if (field.getType().equals(method.getReturnType())){
                    method.setAccessible(true);
                    field.setAccessible(true);
                    writeMessage(field.getName() + " -> ");
                    field.set(model,method.invoke(ch));
                }
            }
        }
        return model;
    }

}
