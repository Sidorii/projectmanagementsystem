package Practics_Module_2.homework.view;

import java.io.*;
import java.sql.Date;
import java.util.Scanner;


/**
 * Created by Иван on 02.12.2016.
 */
public class ConsoleHelper {

    private static final Scanner console = new Scanner(new InputStreamReader(System.in));


    public static void writeMessage(String str){
        System.out.print(str);
    }

    public static String readLine() throws IOException {
        String out = console.nextLine();
        if (out.equals("")){
            writeMessage("Строка не учитывается.\n");
            return null;
        }else
            return out;
    }


    public static Integer readInt() {
        Integer number = null;
        try {
            number = Integer.parseInt(console.nextLine());
        }catch (NumberFormatException exc){
            writeMessage("Строка не учитываеться.\n");
        }
        return number;
    }

    public static Date readDate(){
        Date date = null;
        try {
            String d = console.nextLine();
            if (!d.equals(""))
                date = Date.valueOf(d);
        }catch (NumberFormatException exc){
            writeMessage("Строка не учитываеться.\n");
        }
        return date;
    }
}
