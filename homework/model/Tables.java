package Practics_Module_2.homework.model;

/**
 * Created by Иван on 03.12.2016.
 */
public enum Tables {

    COMPANIES(4),DEVELOPERS(6),PROJECTS(7),SKILLS(3),CUSTOMERS(3);

    int numberOfFields;

    Tables(int numberOfFields){
        this.numberOfFields = numberOfFields;
    }

    public int getNumberOfFields() {
        return numberOfFields;
    }
}
