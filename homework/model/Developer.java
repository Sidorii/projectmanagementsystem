package Practics_Module_2.homework.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;


public class Developer implements Model{


    public static final Tables TABLE_NAME = Tables.DEVELOPERS;
    @Field
    private Integer dev_id;
    @Field
    private String name;
    @Field
    private Integer experience;
    @Field
    private Integer age;
    @Field
    private Integer project_id;
    @Field
    private Integer salary;

    public Developer(){
        dev_id = null;
        name = null;
        experience = null;
        age = null;
        project_id = null;
        salary = null;
    }

    public Developer(Integer id, String name, Integer experience, Integer age, Integer project_id, Integer salary) {
        this.dev_id = id;
        this.name = name;
        this.experience = experience;
        this.age = age;
        this.project_id = project_id;
        this.salary = salary;
    }

    public Developer(ResultSet resultSet) throws SQLException{
        dev_id = resultSet.getInt("dev_id");
        name = resultSet.getString("name");
        experience = resultSet.getInt("experience");
        age = resultSet.getInt("age");
        project_id = resultSet.getInt("project_id");
        salary = resultSet.getInt("salary");
    }
    public Integer getDev_id() {
        return dev_id;
    }

    public void setDev_id(Integer dev_id) {
        this.dev_id = dev_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getProject_id() {
        return project_id;
    }

    public void setProject_id(Integer project_id) {
        this.project_id = project_id;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public void initializeStatementParameters(PreparedStatement p)throws SQLException {

        if (dev_id != null)
            p.setLong(1, dev_id);
        else
            p.setNull(1, Types.INTEGER);
        if(name != null)
            p.setString(2,name);
        else
            p.setNull(2,Types.VARCHAR);
        if (experience != null)
            p.setLong(3,experience);
        else
            p.setNull(3, Types.INTEGER);
        if (age != null)
            p.setLong(4,age);
        else
            p.setNull(4, Types.INTEGER);
        if (project_id != null)
            p.setLong(5,project_id);
        else
            p.setNull(5, Types.INTEGER);
        if (salary!= null)
            p.setLong(6,salary);
        else
            p.setNull(6, Types.INTEGER);
    }

    @Override
    public String toString() {
        return "Developer{" +
                "dev_id=" + dev_id +
                ", name='" + name + '\'' +
                ", experience=" + experience +
                ", age=" + age +
                ", project_id=" + project_id +
                ", salary=" + salary +
                '}';
    }

    @Override
    public Tables getTable() {
        return TABLE_NAME;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Developer developer = (Developer) o;

        if (!dev_id.equals(developer.dev_id)) return false;
        if (!name.equals(developer.name)) return false;
        if (!experience.equals(developer.experience)) return false;
        if (!age.equals(developer.age)) return false;
        if (!project_id.equals(developer.project_id)) return false;
        return salary.equals(developer.salary);

    }

    @Override
    public int hashCode() {
        int result = dev_id.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + experience.hashCode();
        result = 31 * result + age.hashCode();
        result = 31 * result + project_id.hashCode();
        result = 31 * result + salary.hashCode();
        return result;
    }

    @Override
    public Model createInstance(ResultSet resultSet) throws SQLException {
        return new Developer(resultSet);
    }
}
