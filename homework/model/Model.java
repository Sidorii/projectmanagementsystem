package Practics_Module_2.homework.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public interface Model {

    void initializeStatementParameters(PreparedStatement p) throws SQLException;

    Tables getTable();

    Model createInstance(ResultSet resultSet) throws SQLException;
}
