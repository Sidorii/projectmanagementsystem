package Practics_Module_2.homework.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 * Created by Иван on 02.12.2016.
 */
public class Customer implements Model {

    public static final Tables TABLE_NAME = Tables.CUSTOMERS;
    @Field
    private Integer customer_id;
    @Field
    private String name;
    @Field
    private Integer balance;


    public Customer(){
        customer_id = null;
        name = null;
        balance = null;
    }

    public Customer(Integer id, String name, Integer balance) {
        this.customer_id = id;
        this.name = name;
        this.balance = balance;
    }

    public Customer(ResultSet resultSet) throws SQLException{
        customer_id = resultSet.getInt("customer_id");
        name = resultSet.getString("name");
        balance = resultSet.getInt("balance");
    }

    public Integer getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Integer customer_id) {
        this.customer_id = customer_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }


    public void initializeStatementParameters(PreparedStatement p)throws SQLException {

        if (customer_id != null)
            p.setLong(1, customer_id);
        else
            p.setNull(1, Types.INTEGER);
        if(name != null)
            p.setString(2,name);
        else
            p.setNull(2,Types.VARCHAR);
        if (balance != null)
            p.setInt(3,balance);
        else
            p.setNull(3,Types.VARCHAR);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customer_id=" + customer_id +
                ", name='" + name + '\'' +
                ", balance=" + balance +
                '}';
    }

    @Override
    public Tables getTable() {
        return TABLE_NAME;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        if (!customer_id.equals(customer.customer_id)) return false;
        if (!name.equals(customer.name)) return false;
        return balance.equals(customer.balance);

    }

    @Override
    public int hashCode() {
        int result = customer_id.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + balance.hashCode();
        return result;
    }

    @Override
    public Model createInstance(ResultSet resultSet) throws SQLException {
        return new Customer(resultSet);
    }
}
