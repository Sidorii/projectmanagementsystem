package Practics_Module_2.homework.model;

import java.sql.*;

/**
 * Created by Иван on 02.12.2016.
 */
public class Project implements Model {

    public static final Tables TABLE_NAME = Tables.PROJECTS;
    @Field
    private Integer project_id;
    @Field
    private String name;
    @Field
    private Date deadline;
    @Field
    private String description;
    @Field
    private Integer company_id;
    @Field
    private Integer customer_id;
    @Field
    private Integer cost;

    public Project(){
        project_id = null;
        name = null;
        deadline = null;
        description = null;
        company_id = null;
        customer_id = null;
    }


    public Project(Integer id, String name, Date deadline, String description, Integer company_id, Integer customer_id, Integer cost) {
        this.project_id = id;
        this.name = name;
        this.deadline = deadline;
        this.description = description;
        this.company_id = company_id;
        this.customer_id = customer_id;
        this.cost = cost;
    }

    public Project(ResultSet resultSet)throws SQLException{
        project_id = resultSet.getInt("project_id");
        name = resultSet.getString("name");
        deadline = resultSet.getDate("deadline");
        description = resultSet.getString("description");
        company_id = resultSet.getInt("company_id");
        customer_id = resultSet.getInt("customer_id");
        cost = resultSet.getInt("cost");
    }

    public Integer getProject_id() {
        return project_id;
    }

    public void setProject_id(Integer project_id) {
        this.project_id = project_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCompany_id() {
        return company_id;
    }

    public void setCompany_id(Integer company_id) {
        this.company_id = company_id;
    }

    public Integer getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Integer customer_id) {
        this.customer_id = customer_id;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public void initializeStatementParameters(PreparedStatement p)throws SQLException {

        if (project_id != null)
            p.setLong(1, project_id);
        else
            p.setNull(1, Types.INTEGER);

        if(name != null)
            p.setString(2,name);
        else
            p.setNull(2,Types.VARCHAR);

        if (deadline != null)
            p.setDate(3,deadline);
        else
            p.setNull(3,Types.VARCHAR);

        if (description != null)
            p.setString(4,description);
        else
            p.setNull(4,Types.VARCHAR);

        if (company_id != null)
            p.setLong(5,company_id);
        else
            p.setNull(5, Types.INTEGER);

        if (customer_id != null)
            p.setLong(6,customer_id);
        else
            p.setNull(6, Types.INTEGER);

        if (cost != null)
            p.setLong(7,cost);
        else
            p.setNull(7, Types.INTEGER);
    }

    @Override
    public String toString() {
        return "Project{" +
                "project_id=" + project_id +
                ", name='" + name + '\'' +
                ", deadline=" + deadline +
                ", description='" + description + '\'' +
                ", company_id=" + company_id +
                ", customer_id=" + customer_id +
                ", cost=" + cost +
                '}';
    }

    @Override
    public Tables getTable() {
        return TABLE_NAME;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Project project = (Project) o;

        if (!project_id.equals(project.project_id)) return false;
        if (!name.equals(project.name)) return false;
        if (!deadline.equals(project.deadline)) return false;
        if (!description.equals(project.description)) return false;
        if (!company_id.equals(project.company_id)) return false;
        if (!customer_id.equals(project.customer_id)) return false;
        return cost.equals(project.cost);

    }

    @Override
    public int hashCode() {
        int result = project_id.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + deadline.hashCode();
        result = 31 * result + description.hashCode();
        result = 31 * result + company_id.hashCode();
        result = 31 * result + customer_id.hashCode();
        result = 31 * result + cost.hashCode();
        return result;
    }

    @Override
    public Model createInstance(ResultSet resultSet) throws SQLException {
        return new Project(resultSet);
    }
}
