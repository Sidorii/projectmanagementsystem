package Practics_Module_2.homework.model;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Иван on 04.12.2016.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Field{

}