package Practics_Module_2.homework.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 * Created by Иван on 02.12.2016.
 */
public class Company implements Model {

    public static final Tables TABLE_NAME = Tables.COMPANIES;
    @Field
    private Integer company_id;
    @Field
    private String name;
    @Field
    private String location;
    @Field
    private String description;

    public Company() {
        company_id = null;
        name = null;
        location = null;
        description = null;
    }

    public Company(Integer id, String name, String location, String description) {
        this.company_id = id;
        this.name = name;
        this.location = location;
        this.description = description;
    }

    public Company(ResultSet resultSet) throws SQLException{
        company_id = resultSet.getInt("company_id");
        name = resultSet.getString("name");
        location = resultSet.getString("location");
        description = resultSet.getString("description");
    }

    public Integer getCompany_id() {
        return company_id;
    }

    public void setCompany_id(Integer id) {
        this.company_id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void initializeStatementParameters(PreparedStatement p)throws SQLException {

        if (company_id != null)
            p.setLong(1,company_id);
        else
            p.setNull(1, Types.INTEGER);
        if(name != null)
            p.setString(2,name);
        else
            p.setNull(2,Types.VARCHAR);
        if (location != null)
            p.setString(3,location);
        else
            p.setNull(3,Types.VARCHAR);
        if (description != null)
            p.setString(4,description);
        else
            p.setNull(4,Types.VARCHAR);
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + company_id +
                ", name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public Tables getTable() {
        return TABLE_NAME;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Company company = (Company) o;

        if (!company_id.equals(company.company_id)) return false;
        if (!name.equals(company.name)) return false;
        if (!location.equals(company.location)) return false;
        return description.equals(company.description);

    }

    @Override
    public int hashCode() {
        int result = company_id.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + location.hashCode();
        result = 31 * result + description.hashCode();
        return result;
    }

    @Override
    public Model createInstance(ResultSet resultSet) throws SQLException {
        return new Company(resultSet);
    }
}
