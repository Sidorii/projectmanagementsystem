package Practics_Module_2.homework.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 * Created by Иван on 02.12.2016.
 */
public class Skill implements Model {


    public static final Tables TABLE_NAME = Tables.SKILLS;
    @Field
    private Integer skill_id;
    @Field
    private String name;
    @Field
    private String description;

    public Skill(){}


    public Skill(ResultSet resultSet) throws SQLException{
        skill_id = resultSet.getInt("skill_id");
        name = resultSet.getString("name");
        description = resultSet.getString("description");
    }

    public Skill(Integer id, String name, String description) {
        this.skill_id = id;
        this.name = name;
        this.description = description;
    }

    public Integer getSkill_id() {
        return skill_id;
    }

    public void setSkill_id(Integer skill_id) {
        this.skill_id = skill_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void initializeStatementParameters(PreparedStatement p)throws SQLException {

        if (skill_id != null)
            p.setLong(1, skill_id);
        else
            p.setNull(1, Types.INTEGER);
        if(name != null)
            p.setString(2,name);
        else
            p.setNull(2,Types.VARCHAR);
        if (description != null)
            p.setString(3,description);
        else
            p.setNull(3,Types.VARCHAR);
    }

    @Override
    public String toString() {
        return "Skill{" +
                "skill_id=" + skill_id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public Tables getTable() {
        return TABLE_NAME;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Skill skill = (Skill) o;

        if (!skill_id.equals(skill.skill_id)) return false;
        if (!name.equals(skill.name)) return false;
        return description.equals(skill.description);

    }

    @Override
    public int hashCode() {
        int result = skill_id.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + description.hashCode();
        return result;
    }

    @Override
    public Model createInstance(ResultSet resultSet) throws SQLException {
        return new Skill(resultSet);
    }
}
